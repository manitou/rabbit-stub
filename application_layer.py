from containers import AmqpQueue
from utils import Consumer


class Broker:
    def __init__(self):
        self.queues = {}
        self.consumers = {}

    def declare_queue(self, routing_key):
        if routing_key not in self.queues:
            self.queues[routing_key] = AmqpQueue(routing_key)
        return self.queues[routing_key]

    def delete_queue(self, routing_key):
        if routing_key not in self.queues:
            print(f'Warning: deleting non-existent queue: {routing_key}')
            return
        # TODO: ensure ack on deleted queue does not crash broker
        del self.queues[routing_key]

    def publish_queue(self, routing_key, message):
        if routing_key not in self.queues:
            # TODO: add some error response
            print(f'Warning: publishing to non-existent queue: {routing_key}')
            return
        self.queues[routing_key].push(message)

    def subscribe_queue(self, routing_key, channel, consumer_tag=None):
        if routing_key not in self.queues:
            # TODO: add some error response
            print(f'Warning: publishing to non-existent queue: {routing_key}')
            return
        consumer = Consumer(channel, consumer_tag)
        self.queues[routing_key].subscribe(consumer)

    def cancel_queue(self, consumer_tag, routing_key=None):
        if routing_key:
            if routing_key not in self.queues:
                # TODO: add some error response
                print(f'Warning: unsubscribing to non-existent queue: {routing_key}')
                return
            self.queues[routing_key].cancel(consumer_tag)
        else:
            for queue in self.queues.values():
                queue.cancel(consumer_tag)


    # make queues, notifications
    # make exchange, notifications