import amqp

with amqp.Connection('localhost:8888') as c:
    ch = c.channel()
    messages_count = 0
    def on_message(message):
        global messages_count
        print('Received message (delivery tag: {}): {}'.format(message.delivery_tag, message.body))
        ch.basic_ack(message.delivery_tag)
        messages_count += 1
        if messages_count >= 3:
            ch.basic_cancel(message.delivery_info['consumer_tag'])
    ch.basic_consume(queue='test1', callback=on_message)
    while messages_count < 3:
        c.drain_events()