import asyncio

from application_layer import Broker
from protocol_layer import AmqpBrokerProtocol

index = 2
broker = Broker()


def protocol_factory():
    global index
    global broker
    protocol = AmqpBrokerProtocol(index, broker=broker)
    index += 1
    return protocol


async def main():
    loop = asyncio.get_running_loop()

    server = await loop.create_server(
        protocol_factory,
        '127.0.0.1', 8888)

    async with server:
        await server.serve_forever()


asyncio.run(main())
