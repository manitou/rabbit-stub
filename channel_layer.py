from pamqp import body, commands, header

from constants import ConnectionStates
from containers import IndexQueue
from utils import MessageContainer, generate_tag


class Channel:
    def __init__(self, channel_id, connection, broker, **kwargs):
        self.channel_id = channel_id
        self.connection = connection
        self.broker = broker
        self.message = None
        self.deliveries = IndexQueue()
        self.consumers = {}

    def handle_frame(self, frame):
        print(f'Channel {self.channel_id} handling frame: {frame}')
        frame_type = frame.__class__.name.replace('.', '_')
        handler = getattr(self, f'handle_{frame_type}', None)
        if not handler:
            print(f'Warning {frame_type} is not handled')
            return
        handler(frame)

    def send_frames(self, frames):
        self.connection.protocol.send_frames(frames, self.channel_id)

    def deliver(self, routing_key, message, delivery_tag, consumer_tag):
        frames = [commands.Basic.Deliver(consumer_tag=consumer_tag, delivery_tag=delivery_tag, routing_key=routing_key), *message.content]
        self.connection.protocol.send_frames(frames, self.channel_id)

    def handle_Queue_Declare(self, frame):
        queue = self.broker.declare_queue(frame.queue)
        response = commands.Queue.DeclareOk(
            queue=queue.routing_key, message_count=len(queue.q), consumer_count=len(queue.consumers))
        self.connection.protocol.send_frames([response], self.channel_id)

    def handle_Basic_Publish(self, frame):
        print(f'Received Publish request. Collecting message')
        self.message = MessageContainer(frame)

    def handle_ContentHeader(self, frame):
        print('Content Header received')
        self.message.append(frame)

    def handle_ContentBody(self, frame):
        print('Content Body received')
        self.message.append(frame)
        if self.message.is_message_ready():
            handler = getattr(self, f'do_actual_{self.message.method_name}', None)
            if not handler:
                print(f'Warning: don\'t know how to handle message {self.message.method_name}')
                return
            message, self.message = self.message, None
            handler(message)

    def do_actual_Publish(self, message):
        routing_key = message.method.routing_key
        print(f'Publishing message {message} to topic "{routing_key}"')
        self.broker.publish_queue(routing_key, message)

    def handle_Basic_Consume(self, frame):
        print(f'Received Consume request')
        routing_key = frame.queue
        consumer_tag = frame.consumer_tag or generate_tag()
        self.send_frames([commands.Basic.ConsumeOk(consumer_tag=consumer_tag)])
        self.broker.subscribe_queue(routing_key=routing_key, channel=self, consumer_tag=consumer_tag)
        self.consumers[consumer_tag] = routing_key

    def do_close(self):
        print(f'Closing channel {self.channel_id}')
        for consume_func in self.deliveries.values():
            consume_func(False)

    def handle_Basic_Ack(self, frame):
        print(f'Received ACK')
        if frame.delivery_tag not in self.deliveries:
            print(f'Warning: ACK for unknown delivery_tag: {frame.delivery_tag}')
            return
        consume_func = self.deliveries[frame.delivery_tag]
        consume_func(True)
        del self.deliveries[frame.delivery_tag]

    def handle_Basic_Cancel(self, frame):
        print(f'Basic Cancel received')
        if frame.consumer_tag not in self.consumers:
            print(f'Warning: consumer tag {frame.consumer_tag} is not registered')
            return
        routing_key = self.consumers.pop(frame.consumer_tag)
        self.broker.cancel_queue(frame.consumer_tag, routing_key)
        self.send_frames([commands.Basic.CancelOk(consumer_tag=frame.consumer_tag)])


class Channel0(Channel):
    def handle_ProtocolHeader(self, frame):
        if self.connection.state != ConnectionStates.START:
            print(f'Warning: received AMQP in wrong state: {self.connection.state}')
            return
        print(f'Received AMQP. Sending START')
        self.send_frames([commands.Connection.Start()])
        self.connection.state = ConnectionStates.AMQP

    def handle_Connection_StartOk(self, frame):
        if self.connection.state != ConnectionStates.AMQP:
            print(f'Warning: received StartOk in wrong state: {self.connection.state}')
            return
        print("Received StartOK. Sending TUNE")
        self.send_frames([commands.Connection.Tune(channel_max=2047, frame_max=131072, heartbeat=60)])
        self.connection.state = ConnectionStates.START_OK

    def handle_Connection_TuneOk(self, frame):
        if self.connection.state != ConnectionStates.START_OK:
            print(f'Warning: received TuneOk in wrong state: {self.connection.state}')
            return
        print('Received TuneOK')
        self.connection.state = ConnectionStates.TUNE_OK

    def handle_Connection_Open(self, frame):
        if self.connection.state != ConnectionStates.TUNE_OK:
            print(f'Warning: received Open in wrong state: {self.connection.state}')
            return
        print(f'Received OPEN: {frame}')
        self.send_frames([commands.Connection.OpenOk()])
        self.connection.state = ConnectionStates.OPENED

    def handle_Connection_OpenOk(self, frame):
        if self.connection.state != ConnectionStates.TUNE_OK:
            print(f'Warning: received OpenOk in wrong state: {self.connection.state}')
            return
        print(f'Received OPEN OK: {frame}')
        self.connection.state = ConnectionStates.OPENED

    def handle_Connection_Close(self, frame):
        self.send_frames([commands.Connection.CloseOk()])
        self.connection.close()
