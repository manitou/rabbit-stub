RabbitMQ Stub
-------------------------
Asyncio stub for replacing RabbitMq instance. Intended to be run from pytest, can have message publishers-subscribers from test directly.


Install
------------------------
 - `poetry install`


Running
------------------------
 1. `python main.py` - to start RabbitMq service (to be updated, will be moved to some usable interface)
 2. `python test_sender.py` - declares queue and sends message to it
 3. `python test_receiver.py` - subscribes to queue and waits for 3 messages
