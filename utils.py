import uuid

from pamqp import header


def format_frame(frame):
    args = {k: getattr(frame, k, None) for k in getattr(frame, '__slots__', [])}
    return f'<{frame.__class__.name}: {args}>'


def generate_tag():
    return uuid.uuid4().hex


class Consumer:
    def __init__(self, channel, consumer_tag=None):
        self.consumer_tag = consumer_tag
        self.channel = channel

    def deliver(self, routing_key, message, consume_func):
        delivery_tag = self.channel.deliveries.append(consume_func)
        self.channel.deliver(routing_key, message, delivery_tag, self.consumer_tag)
        return delivery_tag


class MessageContainer:
    def __init__(self, method_frame):
        self.method = method_frame
        self.content = []
        self.body_size = None
        self.method_name = method_frame.__class__.__name__
        print(f'==== MessageContainer: method={self.method_name}')

    def append(self, frame):
        if not self.content and not isinstance(frame, header.ContentHeader):
            raise Exception('First message frame is not ContentHeader')
        if not self.content and isinstance(frame, header.ContentHeader):
            self.body_size = frame.body_size
        self.content.append(frame)
        print(f'==== MessageContainer (append): body_size={self.body_size}, message_size={sum([len(f) for f in self.content[1:]])}')

    def is_message_ready(self):
        if self.body_size is None:
            return False
        collected_message_size = sum([len(f) for f in self.content[1:]])
        return collected_message_size >= self.body_size

    def __str__(self):
        return f'<Message: size={self.body_size}>'