import amqp

if __name__ == '__main__':
    with amqp.Connection('localhost:8888') as c:
        ch = c.channel()
        ch.queue_declare('test1')
        ch.basic_publish(amqp.Message('Hello World'), routing_key='test1')
