from pamqp import commands
from pamqp.base import Frame

from channel_layer import Channel, Channel0
from constants import ConnectionStates


class OpenOk(commands.Channel.OpenOk):
    __annotations__ = {'channel_id': int}
    _channel_id = 'long'

    def validate(self) -> None:
        pass


class Connection:
    def __init__(self, protocol, channel_class=Channel, **kwargs):
        self.protocol = protocol
        self.state = ConnectionStates.START
        self.channel0 = Channel0(0, self, **kwargs)
        self.channel_class = channel_class
        self.channels = {
            0: self.channel0
        }
        self.kwargs = kwargs

    def connected(self, peername):
        pass

    def new_channel_request(self, frame, channel_id):
        if self.state != ConnectionStates.OPENED:
            print(f'Cannot establish new channel in this state: {self.state}')
            return False
        return isinstance(frame, commands.Channel.Open) and channel_id not in self.channels

    def frame_received(self, frame: Frame, channel_id):
        channel = self.channels.get(channel_id, None)
        if not channel:
            if self.new_channel_request(frame, channel_id):
                print(f'Establishing new channel: {channel_id}')
                self.channels[channel_id] = self.channel_class(channel_id, self, **self.kwargs)
                self.protocol.send_frames([OpenOk(channel_id=0)], channel_id)
                return
            print(f'Warning: request on unhandled channel: {channel_id}: {frame}')
            return
        channel.handle_frame(frame)

    def close(self):
        print('Closing connection by demand')
        for channel in self.channels.values():
            channel.do_close()
        self.protocol.transport.close()
        self.protocol = None
