from collections import deque, namedtuple


class IndexQueue:
    def __init__(self, _list=None):
        self.top = -1
        self.q = deque()
        self.data = {}
        if _list:
            for item in _list:
                self.append(item)

    def append(self, value):
        self.top += 1
        self.data[self.top] = value
        self.q.append(self.top)
        return self.top

    def popleft(self):
        i = self.q.popleft()
        return self.data.pop(i)

    def __len__(self):
        return len(self.q)

    def __getitem__(self, item):
        return self.data[item]

    def get(self, key, default=None):
        if key not in self.data:
            if default is None:
                raise KeyError(key)
            return default
        return self.data[key]

    def __setitem__(self, key, value):
        if key not in self.data:
            raise KeyError("Cannot modify non-existent element")
        self.data[key] = value

    def __delitem__(self, key):
        del self.data[key]
        self.q.remove(key)

    def __iter__(self):
        return self.q.__iter__()

    def items(self):
        for index in self.q:
            yield index, self.data[index]

    def values(self):
        for index in self.q:
            yield self.data[index]

    def __bool__(self):
        return len(self.q) > 0

    def __contains__(self, item):
        return item in self.data


class ConsumersRing:
    def __init__(self):
        self.q = deque()
        self.data = {}

    def append(self, key, value):
        self.data[key] = value
        self.q.append(key)

    def __len__(self):
        return len(self.q)

    def __getitem__(self, item):
        return self.data[item]

    def __setitem__(self, key, value):
        if key not in self.data:
            raise KeyError("Cannot modify non-existent element")
        self.data[key] = value

    def __delitem__(self, key):
        del self.data[key]
        self.q.remove(key)

    def __iter__(self):
        return self.q.__iter__()

    def items(self):
        for index in self.q:
            yield index, self.data[index]

    def rotate(self, n):
        self.q.rotate(n)

    def front(self):
        return self.data[self.q[0]]

    def get_and_rotate(self):
        top = self.front()
        self.rotate(1)
        return top

    def __bool__(self):
        return len(self.q) > 0


class AmqpQueue:
    Item = namedtuple('QueueItem', 'message consumer delivery_tag')

    def __init__(self, routing_key):
        self.routing_key = routing_key
        self.q = IndexQueue()
        self.consumers = ConsumersRing()

    def push(self, value):
        self.q.append(self.Item(value, None, None))
        self._propagate()

    def _are_items_to_propagate(self):
        return self._get_index_to_propagate() is not None

    def _get_index_to_propagate(self):
        for index in self.q:
            if self.q[index].consumer is None:
                return index

    def _make_consume_func(self, index):
        def consume_func(delivered):
            if delivered:
                print(f'Message {self.q[index]} delivered')
                # TODO: check delivery_tag here
                del self.q[index]
                self._propagate()
                # TODO: control number of messages delivered to one consumer
            else:
                print(f'Warning: Message {self.q[index]} not delivered')
                self.q[index] = self.Item(self.q[index].message, None, None)
        return consume_func

    def _propagate(self):
        print(f'Propagate: consumers={bool(self.consumers)}, items={bool(self._are_items_to_propagate())}')
        if not self.consumers or not self._are_items_to_propagate():
            return
        while self._are_items_to_propagate():
            index = self._get_index_to_propagate()
            consumer = self.consumers.get_and_rotate()
            delivery_tag = consumer.deliver(self.routing_key, self.q[index].message, self._make_consume_func(index))
            self.q[index] = self.Item(self.q[index].message, consumer, delivery_tag)

    def subscribe(self, consumer):
        self.consumers.append(consumer.consumer_tag, consumer)
        self._propagate()

    def cancel(self, consumer_tag):
        for index in self.q:
            if self.q[index].consumer.consumer_tag == consumer_tag:
                print(f'Warning: Cancelling message {self.q[index]} delivery')
                self.q[index] = self.Item(self.q[index].message, None, None)
        del self.consumers[consumer_tag]
