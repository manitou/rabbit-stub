import asyncio

from pamqp.exceptions import UnmarshalingException
from pamqp.frame import marshal, unmarshal

from connection_layer import Connection
from utils import format_frame


class AmqpBrokerProtocol(asyncio.Protocol):
    def __init__(self, id, connection_class=Connection, **kwargs):
        super().__init__()
        self._id = id
        self.connection = connection_class(self, **kwargs)
        self.buffer = None

    def connection_made(self, transport):
        peername = transport.get_extra_info('peername')
        print(f'{self._id}: Connection from {peername}')
        self.transport = transport
        self.buffer = bytes()
        self.connection.connected(peername)

    def data_received(self, data):
        self.buffer += data
        while self.buffer:
            try:
                ret = unmarshal(self.buffer)
            except UnmarshalingException as e:
                print(f'Got exception on data receive: {e}')
                if e.args[1] == 'Not all data received':
                    return
                raise
            byte_count, channel_id, frame = ret
            self.buffer = self.buffer[byte_count:]
            print(f'>>>>>> RECV: {format_frame(frame)}')
            self.connection.frame_received(frame, channel_id)

    def send_frames(self, frames, channel_id):
        for frame in frames:
            print(f'<<<<<< SEND: {format_frame(frame)}')
            self.transport.write(marshal(frame, channel_id))

    def close_connection(self):
        self.transport.close()
